# Data Quality

Code to collect data quality information

Some examples from [itk-reports-reporistory](https://itk-reporting-repository.docs.cern.ch):

- [Module Bow](https://wraight.web.cern.ch/strips-reports/TTDB-S-MODULE-MODULE_BOW.html)
- [Module Metrology](https://wraight.web.cern.ch/strips-reports/TTDB-S-MODULE-MODULE_METROLOGY.html)

