import os
from datetime import datetime
import shutil

#####################
### useful functions
#####################

def GetDateTimeNotice():
    dtArr=["!!! notice",
          f"\tPage generated: {datetime.now().date()} ({datetime.now().time()})",
          "\tFor updates/fixes contact: wraightATcern.ch"
        ]
    ### convert to string
    dtStr=""
    for ia in dtArr:
        dtStr+=f"{ia}\n\n"
    
    return dtStr


#####################
### main function
#####################
def main():

    ### find insteresting directories
    print("Copying READMEs")
    imgFiles=[]
    contentFiles=[]
    ### search folders for READMEs
    for d in sorted(os.listdir(os.getcwd())):
        if os.path.isfile(d+"/README.md"):
            print(f"\tfound directory: {d}")
            fileName=d.split('/')[-1]
            print(f"\twriting: {fileName}")
            shutil.copyfile(d+"/README.md", f"docs/{fileName}.md")
            contentFiles.append(fileName)
            ### check for image files
            if fileName.split('.')[-1].lower() in ['png','svg','png','jpg','jpeg']:
                imgFiles.append(fileName)

    ### write index (with timestamp)
    print("Writing index")
    shutil.copyfile(os.getcwd()+"/README.md", "docs/index.md")
    with open("docs/index.md", "a") as content_file:
        content_file.write(GetDateTimeNotice())

    ### update yml
    # notebooks
    with open("mkdocs.yml", "a") as yml_file:
        for cf in contentFiles:
            newStr=f"  - {cf.replace('-',' ').title()}: {cf+'.md'}\n"
            yml_file.write(newStr)

### for commandline running
if __name__ == "__main__":
    main()
    print("All done.")
