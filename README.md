# cluster-report-mock-up

Example of possible cluster-report layout

Includes infromation on:

1. Inventory

2. Yields

3. Rates

4. Data Quality

---

## Inventory

What you got?

---

## Production Yields

Coming from Vancouver Influx work with regular & automated PDB quaries

---

## Production Rates

Coming from Theo's simulations

---

## Data Quality

Coming form [itk-reports-repository](https://itk-reporting-repository.docs.cern.ch)

